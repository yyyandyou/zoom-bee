<?php
if ($this->session->userdata('Type_' . ucfirst($this->uri->segment(1))) == 'V') {
    include VIEWPATH . 'vendor/header.php';
    $folder_name = 'vendor';
} else {
    include VIEWPATH . 'admin/header.php';
    $folder_name = 'admin';
}
?>
<div class="dashboard-body">
    <!-- Start Content -->
    <input type="hidden" id="folder_name" value="<?php echo $folder_name; ?>"/>
    <div class="content">
        <!-- Start Container -->
        <div class="container-fluid ">
            <section class="form-light px-2 sm-margin-b-20">
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12 m-auto">
                        <?php $this->load->view('message'); ?>
                        <div class="header bg-color-base p-3">
                            <div class="row">
                                <span class="col-md-9 col-9 m-0">
                                    <h3 class="black-text font-bold mb-0"><?php echo translate('my_downline'); ?></h3>
                                </span>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div class="row">
                                         <label class="col-md-9 col-9 m-0">
                                            <?php
                                            $referral_url = base_url('/vendor-register/').$this->session->userdata('Vendor_ID');
                                            echo translate('referral')." ".translate('link').": ".$referral_url; ?>
                                            <!-- The button used to copy the text -->
                                            <button class="btn btn-primary" onclick="myFunction()">Copy text</button>
                                        </label>
                                            <!-- The text field -->
                                            <input type="hidden" value="<?php echo $referral_url;?>" id="myInput">
                                    </div>
                                        <table class="table mdl-data-table" id="example">
                                        <thead>
                                            <tr>
                                                <th class="text-center font-bold dark-grey-text">#</th>
                                                <th class="text-center font-bold dark-grey-text"><?php echo translate('name'); ?></th>
                                                <th class="text-center font-bold dark-grey-text"><?php echo translate('email'); ?></th>
                                                <th class="text-center font-bold dark-grey-text"><?php echo translate('phone'); ?></th>
                                                <th class="text-center font-bold dark-grey-text"><?php echo translate('status'); ?></th>
                                                <th class="text-center font-bold dark-grey-text"><?php echo translate('is_payout'); ?></th>
                                                <th class="text-center font-bold dark-grey-text"><?php echo translate('created_date'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            if (isset($vendor_data) && count($vendor_data) > 0) {
                                                foreach ($vendor_data as $row) {
                                                    ?>
                                                    <tr>
                                                        <td class="text-center"><?php echo $i; ?></td>
                                                        <td class="text-center"><?php echo $row['first_name'] . ' ' . $row['last_name']; ?></td>
                                                        <td class="text-center"><?php echo $row['email']; ?></td>
                                                        <td class="text-center"><?php echo $row['phone']; ?></td>
                                                        <td class="text-center"><?php echo print_vendor_status($row['status']); ?></td>
                                                        <td class="text-center"><?php echo print_vendor_first_payout($row['first_pay']); ?></td>
                                                        <td class="text-center"><?php echo get_formated_date($row['created_on'], "N"); ?></td>
                                                    </tr>
                                                    <?php
                                                    $i++;
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--col-md-12-->
                </div>
                <!--Row-->
            </section>
        </div>
    </div>   
</div>


<script src="<?php echo $this->config->item('js_url'); ?>module/staff.js" type='text/javascript'></script>
    <script>
        function myFunction() {
            var copyText = document.getElementById("myInput");
            var dummy = document.createElement("textarea");
            document.body.appendChild(dummy);
            dummy.value = copyText.value;
            dummy.select();

            dummy.select();
            dummy.setSelectionRange(0, 99999)
            document.execCommand("copy");
            document.body.removeChild(dummy);
        }
    </script>
<?php include VIEWPATH . $folder_name . '/footer.php'; ?>