<?php
$first_name = isset($customer_data['first_name']) ? $customer_data['first_name'] : set_value('first_name');
$last_name = isset($customer_data['last_name']) ? $customer_data['last_name'] : set_value('last_name');
$email = isset($customer_data['email']) ? $customer_data['email'] : set_value('email');
$phone = isset($customer_data['phone']) ? $customer_data['phone'] : set_value('phone');

$profile_image = isset($customer_data["profile_image"]) ? $customer_data['profile_image'] : set_value("profile_image") ;
$country = isset($customer_data["country"]) ? translate($country_title) : set_value("country");
$city = isset($customer_data["city"]) ? translate($city_title) : set_value("city");
$dob = isset($customer_data["dob"]) ? $customer_data['dob'] : set_value("dob");
$interest = isset($customer_data["interest"]) ? $customer_data['interest'] : set_value("interest");
$gender = isset($customer_data["gender"]) ? ucfirst($customer_data['gender']) : set_value("gender");
$race = isset($customer_data["race"]) ? $customer_data['race'] : set_value("race");
$religion = isset($customer_data["religion"]) ? $customer_data['religion'] : set_value("religion");
$relationship = isset($customer_data["relationship"]) ? $customer_data['relationship'] : set_value("relationship");
$children = isset($customer_data["children"]) ? $customer_data['children'] : set_value("children");
$education = isset($customer_data["education"]) ? $customer_data['education'] : set_value("education");
$preferred_language = isset($customer_data["preferred_language"]) ? $customer_data['preferred_language'] : set_value("preferred_language");
$body_type = isset($customer_data["body_type"]) ? $customer_data['body_type'] : set_value("body_type");
$smoke = isset($customer_data["smoke"]) ? $customer_data['smoke'] : set_value("smoke");
$drink = isset($customer_data["drink"]) ? $customer_data['drink'] : set_value("drink");
$staying = isset($customer_data["staying"]) ? $customer_data['staying'] : set_value("staying");

if (isset($customer_data['profile_image']) && file_exists(FCPATH . uploads_path . "/profiles/" . $customer_data['profile_image']) && $customer_data['profile_image'] != '') {
    $img_src = base_url() . uploads_path . "/profiles/" . $customer_data['profile_image'];
} else {
    $img_src = base_url() . img_path . "/user.png";
}
if ($this->session->userdata('Type_' . ucfirst($this->uri->segment(1))) == 'V') {
    include VIEWPATH . 'vendor/header.php';
    $folder_name = 'vendor';
} else {
    include VIEWPATH . 'admin/header.php';
    $folder_name = 'admin';
}
?>
<style>
    .select-wrapper input.select-dropdown {
        color: black;
    }
</style>
<div class="dashboard-body">
    <!-- Start Content -->
    <div class="content">
        <!-- Start Container -->
        <div class="container-fluid">
            <section class="form-light px-2 sm-margin-b-20 ">
                <!-- Row -->
                <div class="row">
                    <div class="col-md-12 m-auto">
                        <?php $this->load->view('message'); ?>

                        <div class="header bg-color-base p-3">
                            <?php if (isset($customer_data['id']) && $customer_data['id'] > 0): ?>
                                <h3 class="black-text mb-0 font-bold"><?php echo translate('update'); ?> <?php echo translate('customer'); ?></h3>
                            <?php else: ?>
                                <h3 class="black-text mb-0 font-bold"><?php echo translate('add'); ?> <?php echo translate('customer'); ?></h3>
                            <?php endif; ?>
                        </div>

                        <div class="card">
                            <div class="card-body resp_mx-0">
                                <?php
                                $attributes = array('id' => 'frmCustomer', 'name' => 'frmCustomer', 'method' => "post");
                                echo form_open_multipart($folder_name . '/save-customer', $attributes);
                                ?>
                                <input type="hidden" name="customer_id" id="customer_id" value="<?php echo isset($customer_data['id']) ? $customer_data['id'] : 0; ?>"/>
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <?php echo form_label(translate('first_name') . ' : <small class ="required">*</small>', 'first_name', array('class' => 'control-label')); ?>
                                            <?php echo form_input(array('autocomplete' => "off", 'id' => 'first_name', 'class' => 'form-control', 'name' => 'first_name', 'value' => $first_name, 'placeholder' => translate('first_name'))); ?>
                                            <?php echo form_error('first_name'); ?>
                                        </div>
                                        <div class="error" id="first_name_validate"></div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <?php echo form_label(translate('last_name') . ' : <small class ="required">*</small>', 'last_name', array('class' => 'control-label')); ?>
                                            <?php echo form_input(array('autocomplete' => "off",'id' => 'last_name', 'class' => 'form-control', 'name' => 'last_name', 'value' => $last_name, 'placeholder' => translate('last_name'))); ?>
                                            <?php echo form_error('last_name'); ?>
                                        </div>
                                        <div class="error" id="last_name_validate"></div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <?php echo form_label(translate('email') . ' : <small class ="required">*</small>', 'email', array('class' => 'control-label')); ?>
                                            <?php echo form_input(array('autocomplete' => "off",'type' => 'email', 'id' => 'email', 'class' => 'form-control', 'name' => 'email', 'value' => $email, 'placeholder' => translate('email'))); ?>
                                            <?php echo form_error('email'); ?>
                                        </div>
                                        <div class="error" id="email_validate"></div>
                                    </div>
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <?php echo form_label(translate('phone') . ' :', 'phone', array('class' => 'control-label')); ?>
                                            <?php echo form_input(array('autocomplete' => "off",'minlength' => "10", 'maxlength' => "10", 'id' => 'phone', 'class' => 'form-control', 'name' => 'phone', 'value' => $phone, 'placeholder' => translate('phone') . ' ' . translate('phone'))); ?>
                                            <?php echo form_error('phone'); ?>
                                        </div>
                                        <div class="error" id="phone_validate"></div>
                                    </div> 

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="Country"> <?php echo translate('city'); ?></label>
                                            <input disabled type="text" id="country" name="country" value="<?php echo $country; ?>" class="form-control" placeholder="<?php echo translate('city'); ?>">
                                            <?php //echo form_error('country'); ?>

                                        </div>
                                        <!--                                    <div class="error" id="country_validate"></div>-->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="City"> <?php echo translate('location'); ?></label>
                                            <input disabled type="text" id="city" name="city" value="<?php echo $city; ?>" class="form-control integers" placeholder="<?php echo translate('location'); ?>" maxlength="10" minlength="10">
                                            <?php //echo form_error('phone'); ?>
                                        </div>
                                        <!--                                    <div class="error" id="Phone_validate"></div>-->
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="race"> <?php echo translate('race'); ?></label>
                                            <select tabindex="1" class="kb-select initialized" id="race" name="race">
                                                <option value=""><?php echo translate('select_race'); ?></option>
                                                <option <?php if($race == 'chinese'){ echo 'selected';} ?> value="chinese"><?php echo translate('Chinese'); ?></option>
                                                <option <?php if($race == 'malay'){ echo 'selected';} ?> value="malay"><?php echo translate('Malay'); ?></option>
                                                <option <?php if($race == 'indian'){ echo 'selected';} ?> value="indian"><?php echo translate('Indian'); ?></option>
                                                <option <?php if($race == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                            </select>
                                            <?php echo form_error('race'); ?>

                                        </div>
                                        <div class="error" id="race_validate"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="religion"> <?php echo translate('religion'); ?></label>
                                            <select tabindex="1" class="kb-select initialized" id="religion" name="religion">
                                                <option value=""><?php echo translate('select_religion'); ?></option>
                                                <option <?php if($religion == 'buddhist'){ echo 'selected';} ?> value="buddhist"><?php echo translate('Buddhist'); ?></option>
                                                <option <?php if($religion == 'catholic'){ echo 'selected';} ?> value="catholic"><?php echo translate('Catholic'); ?></option>
                                                <option <?php if($religion == 'christian'){ echo 'selected';} ?> value="christian"><?php echo translate('Christian'); ?></option>
                                                <option <?php if($religion == 'muslim'){ echo 'selected';} ?> value="muslim"><?php echo translate('Muslim'); ?></option>
                                                <option <?php if($religion == 'hindu'){ echo 'selected';} ?> value="hindu"><?php echo translate('Hindu'); ?></option>
                                                <option <?php if($religion == 'atheist'){ echo 'selected';} ?> value="atheist"><?php echo translate('Atheist'); ?></option>
                                                <option <?php if($religion == 'jewish'){ echo 'selected';} ?> value="jewish"><?php echo translate('Jewish'); ?></option>
                                                <option <?php if($religion == 'agnostic'){ echo 'selected';} ?> value="agnostic"><?php echo translate('Agnostic'); ?></option>
                                                <option <?php if($religion == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                            </select>
                                            <?php echo form_error('religion'); ?>
                                        </div>
                                        <div class="error" id="religion_validate"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="relationship"> <?php echo translate('relationship'); ?></label>
                                            <select tabindex="1" class="kb-select initialized" id="relationship" name="relationship">
                                                <option value=""><?php echo translate('select_relationship'); ?></option>
                                                <option <?php if($relationship == 'single'){ echo 'selected';} ?> value="single"><?php echo translate('Single'); ?></option>
                                                <option <?php if($relationship == 'in_relationship'){ echo 'selected';} ?> value="in_relationship"><?php echo translate('In_Relationship'); ?></option>
                                                <option <?php if($relationship == 'married'){ echo 'selected';} ?> value="married"><?php echo translate('Married'); ?></option>
                                                <option <?php if($relationship == 'divorced'){ echo 'selected';} ?> value="divorced"><?php echo translate('Divorced'); ?></option>
                                                <option <?php if($relationship == 'widow'){ echo 'selected';} ?> value="widow"><?php echo translate('Widow'); ?></option>
                                                <option <?php if($relationship == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                            </select>
                                            <?php echo form_error('relationship'); ?>

                                        </div>
                                        <div class="error" id="relationship_validate"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="children"> <?php echo translate('children'); ?></label>
                                            <select tabindex="1" class="kb-select initialized" id="children" name="children">
                                                <option value=""><?php echo translate('select_children'); ?></option>
                                                <option <?php if($children == 'no'){ echo 'selected';} ?> value="no"><?php echo translate('no'); ?></option>
                                                <option <?php if($children == 'some_day'){ echo 'selected';} ?> value="some_day"><?php echo translate('Some_Day'); ?></option>
                                                <option <?php if($children == 'expecting'){ echo 'selected';} ?> value="expecting"><?php echo translate('Expecting'); ?></option>
                                                <option <?php if($children == 'have_kids'){ echo 'selected';} ?> value="have_kids"><?php echo translate('Have_Kids'); ?></option>
                                                <option <?php if($children == 'have_kids_dont_want'){ echo 'selected';} ?> value="have_kids_dont_want"><?php echo translate('Have_Kids_And_Dont_Want_It'); ?></option>
                                                <option <?php if($children == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                            </select>
                                            <?php echo form_error('children'); ?>
                                        </div>
                                        <div class="error" id="children_validate"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="education"> <?php echo translate('education'); ?></label>
                                            <select tabindex="1" class="kb-select initialized" id="education" name="education">
                                                <option value=""><?php echo translate('select_education'); ?></option>
                                                <option <?php if($education == 'high_school'){ echo 'selected';} ?> value="high_school"><?php echo translate('High_School'); ?></option>
                                                <option <?php if($education == 'college'){ echo 'selected';} ?> value="college"><?php echo translate('College'); ?></option>
                                                <option <?php if($education == 'university'){ echo 'selected';} ?> value="university"><?php echo translate('University'); ?></option>
                                                <option <?php if($education == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                            </select>
                                            <?php echo form_error('education'); ?>

                                        </div>
                                        <div class="error" id="education_validate"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="preferred_language"> <?php echo translate('preferred_language'); ?></label>
                                            <select tabindex="1" class="kb-select initialized" id="preferred_language" name="preferred_language">
                                                <option value=""><?php echo translate('select_preferred_language'); ?></option>
                                                <option <?php if($preferred_language == 'english'){ echo 'selected';} ?> value="english"><?php echo translate('english'); ?></option>
                                                <option <?php if($preferred_language == 'chinese'){ echo 'selected';} ?> value="chinese"><?php echo translate('Mandarin'); ?></option>
                                                <option <?php if($preferred_language == 'malay'){ echo 'selected';} ?> value="malay"><?php echo translate('Bahasa_Malaysia'); ?></option>
                                                <option <?php if($preferred_language == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                            </select>
                                            <?php echo form_error('preferred_language'); ?>
                                        </div>
                                        <div class="error" id="preferred_language_validate"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="body_type"> <?php echo translate('body_type'); ?></label>
                                            <select tabindex="1" class="kb-select initialized" id="body_type" name="body_type">
                                                <option value=""><?php echo translate('select_body_type'); ?></option>
                                                <option <?php if($body_type == 'slim'){ echo 'selected';} ?> value="slim"><?php echo translate('Slim'); ?></option>
                                                <option <?php if($body_type == 'sporty'){ echo 'selected';} ?> value="sporty"><?php echo translate('Sporty'); ?></option>
                                                <option <?php if($body_type == 'curvy'){ echo 'selected';} ?> value="curvy"><?php echo translate('Curvy'); ?></option>
                                                <option <?php if($body_type == 'average'){ echo 'selected';} ?> value="average"><?php echo translate('average'); ?></option>
                                                <option <?php if($body_type == 'above_average'){ echo 'selected';} ?> value="above_average"><?php echo translate('Above Average'); ?></option>
                                                <option <?php if($body_type == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                            </select>
                                            <?php echo form_error('body_type'); ?>

                                        </div>
                                        <div class="error" id="body_type_validate"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="staying"> <?php echo translate('staying'); ?></label>
                                            <select tabindex="1" class="kb-select initialized" id="staying" name="staying">
                                                <option value=""><?php echo translate('select_staying'); ?></option>
                                                <option <?php if($staying == 'alone'){ echo 'selected';} ?> value="alone"><?php echo translate('Alone'); ?></option>
                                                <option <?php if($staying == 'with_parents'){ echo 'selected';} ?> value="with_parents"><?php echo translate('With_Parents'); ?></option>
                                                <option <?php if($staying == 'with_friends'){ echo 'selected';} ?> value="with_friends"><?php echo translate('With_Friends'); ?></option>
                                                <option <?php if($staying == 'with_partner'){ echo 'selected';} ?> value="with_partner"><?php echo translate('With_Partner'); ?></option>
                                                <option <?php if($staying == 'with_children'){ echo 'selected';} ?> value="with_children"><?php echo translate('With_Children'); ?></option>
                                                <option <?php if($staying == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                            </select>
                                            <?php echo form_error('staying'); ?>
                                        </div>
                                        <div class="error" id="staying_validate"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="smoke"> <?php echo translate('smoke'); ?></label>
                                            <select tabindex="1" class="kb-select initialized" id="smoke" name="smoke">
                                                <option value=""><?php echo translate('select_smoke'); ?></option>
                                                <option <?php if($smoke == 'never'){ echo 'selected';} ?> value="never"><?php echo translate('Never'); ?></option>
                                                <option <?php if($smoke == 'always'){ echo 'selected';} ?> value="always"><?php echo translate('Always'); ?></option>
                                                <option <?php if($smoke == 'social_smoker'){ echo 'selected';} ?> value="social_smoker"><?php echo translate('Social_Smoker'); ?></option>
                                            </select>
                                            <?php echo form_error('smoke'); ?>

                                        </div>
                                        <div class="error" id="smoke_validate"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="drink"> <?php echo translate('drink'); ?></label>
                                            <select tabindex="1" class="kb-select initialized" id="drink" name="drink">
                                                <option value=""><?php echo translate('select_drink'); ?></option>
                                                <option <?php if($drink == 'never'){ echo 'selected';} ?> value="never"><?php echo translate('Never'); ?></option>
                                                <option <?php if($drink == 'always'){ echo 'selected';} ?> value="always"><?php echo translate('Always'); ?></option>
                                                <option <?php if($drink == 'social_drinker'){ echo 'selected';} ?> value="social_drinker"><?php echo translate('Social_Drinker'); ?></option>
                                            </select>
                                            <?php echo form_error('drink'); ?>
                                        </div>
                                        <div class="error" id="drink_validate"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label for="Interest"> <?php echo translate('interest'); ?> <small class="required">*</small></label>
                                            <textarea name="interest" id="interest"><?php echo $interest?></textarea>
                                            <?php echo form_error('interest'); ?>
                                        </div>
                                        <div class="error" id="interest_validate"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group resp_mb-0">
                                            <label><?php echo translate('select'); ?> <?php echo translate('image'); ?></label>
                                            <div class="file-field">
                                                <div class="btn btn-primary btn-sm">
                                                    <span><?php echo translate('choose_file'); ?></span>
                                                    <input onchange="readURL(this)" id="imageurl"  type="file" name="profile_image"/>
                                                </div>
                                                <div class="file-path-wrapper" style="padding-top: 4px;">
                                                    <input class="file-path validate form-control readonly" readonly type="text" placeholder="<?php echo translate('upload_your_file'); ?>" >
                                                </div>
                                                <?php echo form_error('profile_image'); ?>
                                            </div>
                                            <div class="error" id="profile_image_validate"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 profile-img">
                                        <div class="form-group resp_mb-0">
                                            <?php
                                            if (isset($customer_data['profile_image']) && file_exists(FCPATH . uploads_path . "/profiles/" . $customer_data['profile_image']) && $customer_data['profile_image'] != '') {
                                                $img_src = base_url() . uploads_path . "/profiles/" . $customer_data['profile_image'];
                                            } else {
                                                $img_src = base_url() . img_path . "/user.png";
                                            }
                                            ?>
                                            <img id="imageurl"  class="img-thumbnail img-fluid p-8p"  style="border-radius:50%;" src="<?php echo $img_src; ?>" alt="<?php echo translate('profile'); ?> <?php echo translate('image'); ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 b-r">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success waves-effect"><?php echo translate('submit'); ?></button>
                                            <a href="<?php echo base_url('admin/customer'); ?>" class="btn btn-info waves-effect"><?php echo translate('cancel'); ?></a>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <!--/Form with header-->
                    </div>
                    <!--Card-->
                </div>
                <!-- End Col -->
            </section>
        </div>
        <!--Row-->
        <!-- End Login-->
    </div>
</div>
<script src="<?php echo $this->config->item('js_url'); ?>module/customer.js" type="text/javascript"></script>
<?php include VIEWPATH . 'admin/footer.php'; ?>
