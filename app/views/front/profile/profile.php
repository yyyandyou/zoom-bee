<?php include VIEWPATH . 'front/header.php'; ?>
<?php
$first_name = (set_value("first_name")) ? set_value("first_name") : $customer_data['first_name'];
$last_name = (set_value("last_name")) ? set_value("last_name") : $customer_data['last_name'];
$email = (set_value("email")) ? set_value("email") : $customer_data['email'];
$phone = (set_value("phone")) ? set_value("phone") : $customer_data['phone'];
$profile_image = set_value("profile_image") ? set_value("profile_image") : $customer_data['profile_image'];
$country = set_value("country") ? set_value("country") : translate($country_title);
$city = set_value("city") ? set_value("city") : translate($city_title);
$dob = (set_value("dob")) ? set_value("dob") : $customer_data['dob'];
$interest = (set_value("interest")) ? set_value("interest") : $customer_data['interest'];
$gender = (set_value("gender")) ? set_value("gender") : ucfirst($customer_data['gender']);
$race = (set_value("race")) ? set_value("race") : $customer_data['race'];
$religion = (set_value("religion")) ? set_value("religion") : $customer_data['religion'];
$relationship = (set_value("relationship")) ? set_value("relationship") : $customer_data['relationship'];
$children = (set_value("children")) ? set_value("children") : $customer_data['children'];
$education = (set_value("education")) ? set_value("education") : $customer_data['education'];
$preferred_language = (set_value("preferred_language")) ? set_value("preferred_language") : $customer_data['preferred_language'];
$body_type = (set_value("body_type")) ? set_value("body_type") : $customer_data['body_type'];
$smoke = (set_value("smoke")) ? set_value("smoke") : $customer_data['smoke'];
$drink = (set_value("drink")) ? set_value("drink") : $customer_data['drink'];
$staying = (set_value("staying")) ? set_value("staying") : $customer_data['staying'];

if (file_exists(FCPATH . uploads_path . "/profiles/" . $customer_data['profile_image']) && $customer_data['profile_image'] != '') {
    $img_src = base_url() . uploads_path . "/profiles/" . $customer_data['profile_image'];
} else {
    $img_src = base_url() . img_path . "/user.png";
}
?>
<!-- Custom Script -->
<script src="<?php echo $this->config->item('js_url'); ?>module/additional-methods.js" type="text/javascript"></script>
<link href="<?php echo $this->config->item('css_url'); ?>module/user_panel.css" rel="stylesheet"/>
<div class="container  mt-20" style="min-height:653px;">
    <div class="row">
        <div class="col-md-4 col-xl-3">
            <div class="card mb-3">

                <div class="card-body text-center">
                    <img src="<?php echo $img_src; ?>" alt="<?php echo $first_name . " " . $last_name; ?>" class="rounded-circle mb-2" width="100" height="100"/>
                    <h4 class="card-title mb-0"><?php echo $first_name . " " . $last_name; ?></h4>
                </div>

                <hr class="my-0">
                <div class="card-body">
                    <nav class="side-menu">
                        <ul class="nav">
                            <li class="active"><a href="<?php echo base_url('profile'); ?>"><span class="fa fa-user"></span> <?php echo translate('profile'); ?></a></li>
                            <li><a href="<?php echo base_url('change-password'); ?>"><span class="fa fa-cog"></span> <?php echo translate('Change_password'); ?></a></li>
                            
                            <?php if (get_site_setting('enable_service') == 'Y'): ?>
                            <li><a href="<?php echo base_url('appointment'); ?>"><span class="fa fa-clock-o"></span> <?php echo translate('my_appointment'); ?></a></li>
                            <?php endif; ?>
                            
                            <?php if (get_site_setting('enable_event') == 'Y'): ?>  
                            <li><a href="<?php echo base_url('event-booking'); ?>"><span class="fa fa-ticket"></span> <?php echo translate('event') . " " . translate('booking'); ?></a></li>
                            <?php endif; ?>
                            
                            <li><a href="<?php echo base_url('payment-history'); ?>"><span class="fa fa-credit-card"></span> <?php echo translate('payment_history'); ?></a></li>
                            <li><a href="<?php echo base_url('logout'); ?>"><span class="fa fa-power-off"></span> <?php echo translate('logout'); ?></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        <div class="col-md-8 col-xl-9">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title mb-0"><?php echo translate('profile'); ?></h5>
                </div>
                <div class="card-body h-100">
                    <div class="row">
                        <div class="col-md-12 m-auto">
                            <?php $this->load->view('message'); ?>
                            <?php
                            $attributes = array('id' => 'UserProfile', 'name' => 'Profile', 'method' => "post");
                            echo form_open_multipart('profile-save', $attributes);
                            ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="first_name"> <?php echo translate('first_name'); ?> <small class="required">*</small></label>
                                        <input type="text" id="first_name" name="first_name" value="<?php echo $first_name; ?>" class="form-control" placeholder="<?php echo translate('first_name'); ?>">                                            
                                        <?php echo form_error('firstname'); ?>

                                    </div>
                                    <div class="error" id="first_name_validate"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="Lastname"> <?php echo translate('last_name'); ?> <small class="required">*</small></label>
                                        <input type="text" id="last_name" name="last_name" value="<?php echo $last_name; ?>" class="form-control" placeholder="<?php echo translate('last_name'); ?>">                                            
                                        <?php echo form_error('last_name'); ?>
                                    </div>
                                    <div class="error" id="last_name_validate"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="Email"> <?php echo translate('email'); ?> <small class="required">*</small></label>
                                        <input disabled type="email" id="email" name="email" value="<?php echo $email; ?>" class="form-control" placeholder="<?php echo translate('email'); ?>">
                                        <?php //echo form_error('email'); ?>

                                    </div>
                                    <div class="error" id="email_validate"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="phone"> <?php echo translate('phone'); ?></label>
                                        <input type="text" id="phone" name="phone" value="<?php echo $phone; ?>" class="form-control integers" placeholder="<?php echo translate('phone'); ?>" maxlength="10" minlength="10">                                            
                                        <?php echo form_error('phone'); ?>
                                    </div>
                                    <div class="error" id="Phone_validate"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="Gender"> <?php echo translate('gender'); ?> </label>
                                        <input disabled type="text" id="gender" name="gender" value="<?php echo $gender; ?>" class="form-control" placeholder="<?php echo translate('gender'); ?>">
                                        <?php //echo form_error('country'); ?>

                                    </div>
                                    <!--                                    <div class="error" id="country_validate"></div>-->
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="Country"> <?php echo translate('dob'); ?> </label>
                                        <input disabled type="text" id="dob" name="dob" value="<?php echo $dob; ?>" class="form-control" placeholder="<?php echo translate('dob'); ?>">
                                        <?php //echo form_error('country'); ?>

                                    </div>
                                    <!--                                    <div class="error" id="country_validate"></div>-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="Country"> <?php echo translate('city'); ?></label>
                                        <input disabled type="text" id="country" name="country" value="<?php echo $country; ?>" class="form-control" placeholder="<?php echo translate('city'); ?>">
                                        <?php //echo form_error('country'); ?>

                                    </div>
<!--                                    <div class="error" id="country_validate"></div>-->
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="City"> <?php echo translate('location'); ?></label>
                                        <input disabled type="text" id="city" name="city" value="<?php echo $city; ?>" class="form-control integers" placeholder="<?php echo translate('location'); ?>" maxlength="10" minlength="10">
                                        <?php //echo form_error('phone'); ?>
                                    </div>
<!--                                    <div class="error" id="Phone_validate"></div>-->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="race"> <?php echo translate('race'); ?></label>
                                        <select tabindex="1" class="kb-select initialized" id="race" name="race">
                                            <option value=""><?php echo translate('select_race'); ?></option>
                                            <option <?php if($race == 'chinese'){ echo 'selected';} ?> value="chinese"><?php echo translate('Chinese'); ?></option>
                                            <option <?php if($race == 'malay'){ echo 'selected';} ?> value="malay"><?php echo translate('Malay'); ?></option>
                                            <option <?php if($race == 'indian'){ echo 'selected';} ?> value="indian"><?php echo translate('Indian'); ?></option>
                                            <option <?php if($race == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                        </select>
                                        <?php echo form_error('race'); ?>

                                    </div>
                                    <div class="error" id="race_validate"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="religion"> <?php echo translate('religion'); ?></label>
                                        <select tabindex="1" class="kb-select initialized" id="religion" name="religion">
                                            <option value=""><?php echo translate('select_religion'); ?></option>
                                            <option <?php if($religion == 'buddhist'){ echo 'selected';} ?> value="buddhist"><?php echo translate('Buddhist'); ?></option>
                                            <option <?php if($religion == 'catholic'){ echo 'selected';} ?> value="catholic"><?php echo translate('Catholic'); ?></option>
                                            <option <?php if($religion == 'christian'){ echo 'selected';} ?> value="christian"><?php echo translate('Christian'); ?></option>
                                            <option <?php if($religion == 'muslim'){ echo 'selected';} ?> value="muslim"><?php echo translate('Muslim'); ?></option>
                                            <option <?php if($religion == 'hindu'){ echo 'selected';} ?> value="hindu"><?php echo translate('Hindu'); ?></option>
                                            <option <?php if($religion == 'atheist'){ echo 'selected';} ?> value="atheist"><?php echo translate('Atheist'); ?></option>
                                            <option <?php if($religion == 'jewish'){ echo 'selected';} ?> value="jewish"><?php echo translate('Jewish'); ?></option>
                                            <option <?php if($religion == 'agnostic'){ echo 'selected';} ?> value="agnostic"><?php echo translate('Agnostic'); ?></option>
                                            <option <?php if($religion == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                        </select>
                                        <?php echo form_error('religion'); ?>
                                    </div>
                                    <div class="error" id="religion_validate"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="relationship"> <?php echo translate('relationship'); ?></label>
                                        <select tabindex="1" class="kb-select initialized" id="relationship" name="relationship">
                                            <option value=""><?php echo translate('select_relationship'); ?></option>
                                            <option <?php if($relationship == 'single'){ echo 'selected';} ?> value="single"><?php echo translate('Single'); ?></option>
                                            <option <?php if($relationship == 'in_relationship'){ echo 'selected';} ?> value="in_relationship"><?php echo translate('In_Relationship'); ?></option>
                                            <option <?php if($relationship == 'married'){ echo 'selected';} ?> value="married"><?php echo translate('Married'); ?></option>
                                            <option <?php if($relationship == 'divorced'){ echo 'selected';} ?> value="divorced"><?php echo translate('Divorced'); ?></option>
                                            <option <?php if($relationship == 'widow'){ echo 'selected';} ?> value="widow"><?php echo translate('Widow'); ?></option>
                                            <option <?php if($relationship == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                        </select>
                                        <?php echo form_error('relationship'); ?>

                                    </div>
                                    <div class="error" id="relationship_validate"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="children"> <?php echo translate('children'); ?></label>
                                        <select tabindex="1" class="kb-select initialized" id="children" name="children">
                                            <option value=""><?php echo translate('select_children'); ?></option>
                                            <option <?php if($children == 'no'){ echo 'selected';} ?> value="no"><?php echo translate('no'); ?></option>
                                            <option <?php if($children == 'some_day'){ echo 'selected';} ?> value="some_day"><?php echo translate('Some_Day'); ?></option>
                                            <option <?php if($children == 'expecting'){ echo 'selected';} ?> value="expecting"><?php echo translate('Expecting'); ?></option>
                                            <option <?php if($children == 'have_kids'){ echo 'selected';} ?> value="have_kids"><?php echo translate('Have_Kids'); ?></option>
                                            <option <?php if($children == 'have_kids_dont_want'){ echo 'selected';} ?> value="have_kids_dont_want"><?php echo translate('Have_Kids_And_Dont_Want_It'); ?></option>
                                            <option <?php if($children == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                        </select>
                                        <?php echo form_error('children'); ?>
                                    </div>
                                    <div class="error" id="children_validate"></div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="education"> <?php echo translate('education'); ?></label>
                                        <select tabindex="1" class="kb-select initialized" id="education" name="education">
                                            <option value=""><?php echo translate('select_education'); ?></option>
                                            <option <?php if($education == 'high_school'){ echo 'selected';} ?> value="high_school"><?php echo translate('High_School'); ?></option>
                                            <option <?php if($education == 'college'){ echo 'selected';} ?> value="college"><?php echo translate('College'); ?></option>
                                            <option <?php if($education == 'university'){ echo 'selected';} ?> value="university"><?php echo translate('University'); ?></option>
                                            <option <?php if($education == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                        </select>
                                        <?php echo form_error('education'); ?>

                                    </div>
                                    <div class="error" id="education_validate"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="preferred_language"> <?php echo translate('preferred_language'); ?></label>
                                        <select tabindex="1" class="kb-select initialized" id="preferred_language" name="preferred_language">
                                            <option value=""><?php echo translate('select_preferred_language'); ?></option>
                                            <option <?php if($preferred_language == 'english'){ echo 'selected';} ?> value="english"><?php echo translate('english'); ?></option>
                                            <option <?php if($preferred_language == 'chinese'){ echo 'selected';} ?> value="chinese"><?php echo translate('Mandarin'); ?></option>
                                            <option <?php if($preferred_language == 'malay'){ echo 'selected';} ?> value="malay"><?php echo translate('Bahasa_Malaysia'); ?></option>
                                            <option <?php if($preferred_language == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                        </select>
                                        <?php echo form_error('preferred_language'); ?>
                                    </div>
                                    <div class="error" id="preferred_language_validate"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="body_type"> <?php echo translate('body_type'); ?></label>
                                        <select tabindex="1" class="kb-select initialized" id="body_type" name="body_type">
                                            <option value=""><?php echo translate('select_body_type'); ?></option>
                                            <option <?php if($body_type == 'slim'){ echo 'selected';} ?> value="slim"><?php echo translate('Slim'); ?></option>
                                            <option <?php if($body_type == 'sporty'){ echo 'selected';} ?> value="sporty"><?php echo translate('Sporty'); ?></option>
                                            <option <?php if($body_type == 'curvy'){ echo 'selected';} ?> value="curvy"><?php echo translate('Curvy'); ?></option>
                                            <option <?php if($body_type == 'average'){ echo 'selected';} ?> value="average"><?php echo translate('average'); ?></option>
                                            <option <?php if($body_type == 'above_average'){ echo 'selected';} ?> value="above_average"><?php echo translate('Above Average'); ?></option>
                                            <option <?php if($body_type == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                        </select>
                                        <?php echo form_error('body_type'); ?>

                                    </div>
                                    <div class="error" id="body_type_validate"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="staying"> <?php echo translate('staying'); ?></label>
                                        <select tabindex="1" class="kb-select initialized" id="staying" name="staying">
                                            <option value=""><?php echo translate('select_staying'); ?></option>
                                            <option <?php if($staying == 'alone'){ echo 'selected';} ?> value="alone"><?php echo translate('Alone'); ?></option>
                                            <option <?php if($staying == 'with_parents'){ echo 'selected';} ?> value="with_parents"><?php echo translate('With_Parents'); ?></option>
                                            <option <?php if($staying == 'with_friends'){ echo 'selected';} ?> value="with_friends"><?php echo translate('With_Friends'); ?></option>
                                            <option <?php if($staying == 'with_partner'){ echo 'selected';} ?> value="with_partner"><?php echo translate('With_Partner'); ?></option>
                                            <option <?php if($staying == 'with_children'){ echo 'selected';} ?> value="with_children"><?php echo translate('With_Children'); ?></option>
                                            <option <?php if($staying == 'other'){ echo 'selected';} ?> value="other"><?php echo translate('other'); ?></option>
                                        </select>
                                        <?php echo form_error('staying'); ?>
                                    </div>
                                    <div class="error" id="staying_validate"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="smoke"> <?php echo translate('smoke'); ?></label>
                                        <select tabindex="1" class="kb-select initialized" id="smoke" name="smoke">
                                            <option value=""><?php echo translate('select_smoke'); ?></option>
                                            <option <?php if($smoke == 'never'){ echo 'selected';} ?> value="never"><?php echo translate('Never'); ?></option>
                                            <option <?php if($smoke == 'always'){ echo 'selected';} ?> value="always"><?php echo translate('Always'); ?></option>
                                            <option <?php if($smoke == 'social_smoker'){ echo 'selected';} ?> value="social_smoker"><?php echo translate('Social_Smoker'); ?></option>
                                        </select>
                                        <?php echo form_error('smoke'); ?>

                                    </div>
                                    <div class="error" id="smoke_validate"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="drink"> <?php echo translate('drink'); ?></label>
                                        <select tabindex="1" class="kb-select initialized" id="drink" name="drink">
                                            <option value=""><?php echo translate('select_drink'); ?></option>
                                            <option <?php if($drink == 'never'){ echo 'selected';} ?> value="never"><?php echo translate('Never'); ?></option>
                                            <option <?php if($drink == 'always'){ echo 'selected';} ?> value="always"><?php echo translate('Always'); ?></option>
                                            <option <?php if($drink == 'social_drinker'){ echo 'selected';} ?> value="social_drinker"><?php echo translate('Social_Drinker'); ?></option>
                                        </select>
                                        <?php echo form_error('drink'); ?>
                                    </div>
                                    <div class="error" id="drink_validate"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label for="Interest"> <?php echo translate('interest'); ?> <small class="required">*</small></label>
                                        <textarea name="interest" id="interest"><?php echo $interest?></textarea>
                                        <?php echo form_error('interest'); ?>
                                    </div>
                                    <div class="error" id="interest_validate"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group resp_mb-0">
                                        <label><?php echo translate('select'); ?> <?php echo translate('image'); ?></label>                                        
                                        <div class="file-field">
                                            <div class="btn btn-primary btn-sm">
                                                <span><?php echo translate('choose_file'); ?></span>
                                                <input onchange="readURL(this)" id="imageurl"  type="file" name="profile_image"/>
                                            </div>
                                            <div class="file-path-wrapper" style="padding-top: 4px;">
                                                <input class="file-path validate form-control readonly" readonly type="text" placeholder="<?php echo translate('upload_your_file'); ?>" >
                                            </div>
                                            <?php echo form_error('profile_image'); ?>
                                        </div>
                                        <div class="error" id="profile_image_validate"></div>
                                    </div>
                                </div>
                                <div class="col-md-6 profile-img">
                                    <div class="form-group resp_mb-0">
                                        <?php
                                        if (file_exists(FCPATH . uploads_path . "/profiles/" . $customer_data['profile_image']) && $customer_data['profile_image'] != '') {
                                            $img_src = base_url() . uploads_path . "/profiles/" . $customer_data['profile_image'];
                                        } else {
                                            $img_src = base_url() . img_path . "/user.png";
                                        }
                                        ?> 
                                        <img id="imageurl"  class="img-thumbnail img-fluid p-8p"  style="border-radius:50%;" src="<?php echo $img_src; ?>" alt="<?php echo translate('profile'); ?> <?php echo translate('image'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <button type="submit" class="btn btn-dark button_common waves-effect"><?php echo translate('update'); ?></button>
                            </div>
                            <?php echo form_close(); ?>
                            <!--/Form with header-->
                        </div>
                        <!-- End Col -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Custom Script -->
<script src="<?php echo $this->config->item('js_url'); ?>module/content.js" type="text/javascript"></script>
<?php include VIEWPATH . 'front/footer.php'; ?>