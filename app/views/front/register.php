<?php include VIEWPATH . 'front/header.php'; ?>
<!-- Start Content -->
<script src="<?php echo $this->config->item('js_url'); ?>module/additional-methods.js" type="text/javascript"></script>

<section class="form-light">
    <div class="container-fluid">
        <!-- Row -->
        <div class="row">
            <div class="col-lg-4 col-md-7 mx-md-auto my-4">

                <div class="card my-3">
                    <div class="header">
                        <h3 class="my-3 text-center"><?php echo translate('user_registration'); ?></h3>
                    </div>

                    <div class="n_page-redirect">
                        <p><?php echo translate('already_created_account?'); ?> <a href="<?php echo base_url("login"); ?>" class="ml-1 font-bold"> <?php echo translate('login'); ?></a></p>
                    </div>

                    <div class="card-body mt-4 resp_mx-0">
                        <?php $this->load->view('message'); ?>
                        <?php
                        $attributes = array('id' => 'Register_user', 'name' => 'Register_user', 'method' => "post");
                        echo form_open_multipart('register-save', $attributes);
                        ?>
                        <input type="hidden" id="next" name="next" value="<?php echo isset($next) ? $next : set_value('next'); ?>"/>
                        <div class="form-group">
                            <label for="first_name"> <?php echo translate('first_name'); ?> <small class="required">*</small></label>
                            <input type="text" value="<?php echo set_value('first_name'); ?>" id="first_name" name="first_name" autocomplete="off"  class="form-control" placeholder="<?php echo translate('first_name'); ?>">                                        
                            <?php echo form_error('first_name'); ?>
                        </div>
                        <div class="error" id="first_name_validate"></div>
                        <div class="form-group">
                            <label for="last_name"> <?php echo translate('last_name'); ?><small class="required">*</small></label>
                            <input type="text"  value="<?php echo set_value('last_name'); ?>" id="last_name" name="last_name"  autocomplete="off" class="form-control" placeholder="<?php echo translate('last_name'); ?>">                                        
                            <?php echo form_error('last_name'); ?>
                        </div>
                        <div class="error" id="last_name_validate"></div>
                        <div class="form-group">
                            <label for="email"> <?php echo translate('email'); ?> <small class="required">*</small></label>
                            <input type="email"  value="<?php echo set_value('email'); ?>" id="email" name="email"  autocomplete="off" class="form-control" placeholder="<?php echo translate('email'); ?>">                                        
                            <?php echo form_error('email'); ?>
                        </div>

                        <div class="error" id="gender_validate"></div>
                        <div class="form-group">
                            <label for="gender"> <?php echo translate('gender'); ?> <small class="required">*</small></label><br>
                            <input type="radio"  id="male" name="gender" value="male" checked="checked">
                            <label for="male"><?php echo translate('male'); ?></label>

                            <input type="radio" id="female" name="gender" value="female">
                            <label for="female"><?php echo translate('female'); ?></label>
                            <?php echo form_error('gender'); ?>
                        </div>

                        <div class="error" id="dob_validate"></div>
                        <div class="form-group">
                            <label for="dob"> <?php echo translate('dob'); ?> <small class="required">*</small></label>
                            <div style="width: 100%;">
                                <div style="float: left; width: 33%; margin-bottom: 1rem">
                                <select tabindex="1" class="kb-select initialized" id="dob_year" name="dob_year">
                                    <option value=""><?php echo translate('year'); ?></option>
                                    <?php
                                        for ($i=2002; $i >= 1940; $i--) {
                                            ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php
                                        }
                                    ?>
                                </select>
                                    <?php echo form_error('dob_year'); ?>
                                </div>

                                <div style="float: left; width: 33%; padding-left: 5px; margin-bottom: 1rem">
                                    <select tabindex="1" class="kb-select initialized" id="dob_month" name="dob_month">
                                        <option value=""><?php echo translate('month'); ?></option>
                                        <option value="01">January</option>
                                        <option value="02">Febuary</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
<!--                                        --><?php
//                                        for ($i=1; $i <= 12; $i++) {
//                                            ?>
<!--                                            <option value="--><?php //echo $i; ?><!--">--><?php //echo $i; ?><!--</option>-->
<!--                                            --><?php
//                                        }
//                                        ?>
                                    </select>
                                    <?php echo form_error('dob_month'); ?>
                                </div>

                                <div style="float: left; width: 33%; padding-left: 5px;margin-bottom: 1rem">
                                    <select tabindex="1" class="kb-select initialized" id="dob_day" name="dob_day">
                                        <option value=""><?php echo translate('day'); ?></option>
                                        <?php
                                        for ($i=1; $i <= 31; $i++) {
                                            ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <?php echo form_error('dob_day'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="error" id="country_validate"></div>
                        <div class="form-group">
                            <label for="country"> <?php echo translate('city'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="country" name="country" onchange="get_location(this.value);">
                                    <option value=""><?php echo translate('select_city'); ?></option>
                                    <option value="3"><?php echo translate('malaysia');?></option>
                                    <option value="5"><?php echo translate('singapore');?></option>
                                </select>
                            <?php echo form_error('country'); ?>
                        </div>

                        <div class="error" id="city_validate"></div>
                        <div class="form-group">
                            <label for="city"> <?php echo translate('location'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="city" name="city">
                                    <option value=""><?php echo translate('select_location'); ?></option>
                                </select>
                                <?php echo form_error('city'); ?>
                        </div>

                        <div class="error" id="race_validate"></div>
                        <div class="form-group">
                            <label for="country"> <?php echo translate('race'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="race" name="race">
                                    <option value=""><?php echo translate('select_race'); ?></option>
                                    <option value="chinese"><?php echo translate('Chinese'); ?></option>
                                    <option value="malay"><?php echo translate('Malay'); ?></option>
                                    <option value="indian"><?php echo translate('Indian');?></option>
                                    <option value="other"><?php echo translate('other');?></option>
                                </select>
                                <?php echo form_error('race'); ?>
                        </div>

                        <div class="error" id="religion_validate"></div>
                        <div class="form-group">
                            <label for="country"> <?php echo translate('religion'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="religion" name="religion">
                                    <option value=""><?php echo translate('select_religion'); ?></option>
                                    <option value="buddhist"><?php echo translate('Buddhist'); ?></option>
                                    <option value="catholic"><?php echo translate('Catholic'); ?></option>
                                    <option value="christian"><?php echo translate('Christian'); ?></option>
                                    <option value="muslim"><?php echo translate('Muslim'); ?></option>
                                    <option value="hindu"><?php echo translate('Hindu'); ?></option>
                                    <option value="atheist"><?php echo translate('Atheist'); ?></option>
                                    <option value="jewish"><?php echo translate('Jewish'); ?></option>
                                    <option value="agnostic"><?php echo translate('Agnostic'); ?></option>
                                    <option value="other"><?php echo translate('other'); ?></option>
                                </select>
                                <?php echo form_error('religion'); ?>
                        </div>

                        <div class="error" id="relationship_validate"></div>
                        <div class="form-group">
                            <label for="country"> <?php echo translate('relationship'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="relationship" name="relationship">
                                    <option value=""><?php echo translate('select_relationship'); ?></option>
                                    <option value="single"><?php echo translate('Single'); ?></option>
                                    <option value="in_relationship"><?php echo translate('In_Relationship'); ?></option>
                                    <option value="married"><?php echo translate('Married'); ?></option>
                                    <option value="divorced"><?php echo translate('Divorced'); ?></option>
                                    <option value="widow"><?php echo translate('Widow'); ?></option>
                                    <option value="other"><?php echo translate('other'); ?></option>
                                </select>
                                <?php echo form_error('relationship'); ?>
                        </div>

                        <div class="error" id="children_validate"></div>
                        <div class="form-group">
                            <label for="country"> <?php echo translate('children'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="children" name="children">
                                    <option value=""><?php echo translate('select_children'); ?></option>
                                    <option value="no"><?php echo translate('no'); ?></option>
                                    <option value="some_day"><?php echo translate('Some_Day'); ?></option>
                                    <option value="expecting"><?php echo translate('Expecting'); ?></option>
                                    <option value="have_kids"><?php echo translate('Have_Kids'); ?></option>
                                    <option value="have_kids_dont_want"><?php echo translate('Have_Kids_And_Dont_Want_It'); ?></option>
                                    <option value="other"><?php echo translate('other'); ?></option>
                                </select>
                                <?php echo form_error('children'); ?>
                        </div>

                        <div class="error" id="education_validate"></div>
                        <div class="form-group">
                            <label for="country"> <?php echo translate('education'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="education" name="education">
                                    <option value=""><?php echo translate('select_education'); ?></option>
                                    <option value="high_school"><?php echo translate('High_School'); ?></option>
                                    <option value="college"><?php echo translate('College'); ?></option>
                                    <option value="university"><?php echo translate('University'); ?></option>
                                    <option value="other"><?php echo translate('other'); ?></option>
                                </select>
                                <?php echo form_error('education'); ?>
                        </div>

                        <div class="error" id="preferred_language_validate"></div>
                        <div class="form-group">
                            <label for="country"> <?php echo translate('preferred_language'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="preferred_language" name="preferred_language">
                                    <option value=""><?php echo translate('select_preferred_language'); ?></option>
                                    <option value="english"><?php echo translate('english'); ?></option>
                                    <option value="chinese"><?php echo translate('Mandarin'); ?></option>
                                    <option value="malay"><?php echo translate('Bahasa_Malaysia'); ?></option>
                                    <option value="other"><?php echo translate('other'); ?></option>
                                </select>
                                <?php echo form_error('preferred_language'); ?>
                        </div>

                        <div class="error" id="body_type_validate"></div>
                        <div class="form-group">
                            <label for="country"> <?php echo translate('body_type'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="body_type" name="body_type">
                                    <option value=""><?php echo translate('select_body_type'); ?></option>
                                    <option value="slim"><?php echo translate('Slim'); ?></option>
                                    <option value="sporty"><?php echo translate('Sporty'); ?></option>
                                    <option value="curvy"><?php echo translate('Curvy'); ?></option>
                                    <option value="average"><?php echo translate('average'); ?></option>
                                    <option value="above_average"><?php echo translate('Above_Average'); ?></option>
                                    <option value="other"><?php echo translate('other'); ?></option>
                                </select>
                                <?php echo form_error('body_type'); ?>
                        </div>

                        <div class="error" id="staying_validate"></div>
                        <div class="form-group">
                            <label for="country"> <?php echo translate('staying'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="staying" name="staying">
                                    <option value=""><?php echo translate('select_staying'); ?></option>
                                    <option value="alone"><?php echo translate('Alone'); ?></option>
                                    <option value="with_parents"><?php echo translate('With_Parents'); ?></option>
                                    <option value="with_friends"><?php echo translate('With_Friends'); ?></option>
                                    <option value="with_partner"><?php echo translate('With_Partner'); ?></option>
                                    <option value="with_children"><?php echo translate('With_Children'); ?></option>
                                    <option value="other"><?php echo translate('other'); ?></option>
                                </select>
                                <?php echo form_error('staying'); ?>
                        </div>

                        <div class="error" id="smoke_validate"></div>
                        <div class="form-group">
                            <label for="country"> <?php echo translate('smoke'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="smoke" name="smoke">
                                    <option value=""><?php echo translate('select_smoke'); ?></option>
                                    <option value="never"><?php echo translate('Never'); ?></option>
                                    <option value="always"><?php echo translate('Always'); ?></option>
                                    <option value="social_smoker"><?php echo translate('Social_Smoker'); ?></option>
                                </select>
                                <?php echo form_error('smoke'); ?>
                        </div>

                        <div class="error" id="drink_validate"></div>
                        <div class="form-group">
                            <label for="country"> <?php echo translate('drink'); ?> <small class="required mr-5px">*</small>
                                <select tabindex="1" class="kb-select initialized" id="drink" name="drink">
                                    <option value=""><?php echo translate('select_drink'); ?></option>
                                    <option value="never"><?php echo translate('Never'); ?></option>
                                    <option value="always"><?php echo translate('Always'); ?></option>
                                    <option value="social_drinker"><?php echo translate('Social_Drinker'); ?></option>
                                </select>
                                <?php echo form_error('drink'); ?>
                        </div>

                        <div class="error" id="interest_validate"></div>
                        <div class="form-group">
                            <label for="interest"> <?php echo translate('interest'); ?> <small class="required mr-5px">*</small>
                                <i class="fa fa-info-circle" tabindex="0" data-html="true" data-toggle="popover" title="<b>Interest</b>" data-content='<span class="d-block"><b> <?php echo translate('info'); ?> - </b></span><span class="d-block">- Separated by common</span>'></i>
                                <textarea name="interest" id="interest"></textarea>
                                <?php echo form_error('interest'); ?>
                        </div>

                        <div class="error" id="email_validate"></div>
                        <div class="form-group">
                            <label for="password"> <?php echo translate('password'); ?> <small class="required mr-5px">*</small>
                                <i class="fa fa-info-circle" tabindex="0" data-html="true" data-toggle="popover" title="<b>Password</b> - Rules" data-content='<span class="d-block"><b> <?php echo translate('info'); ?> - </b></span><span class="d-block">- <?php echo translate('password_length'); ?></span>'></i></label>
                            <input type="password"  id="password" name="password" class="form-control" placeholder="<?php echo translate('password'); ?>">                                        
                            <?php echo form_error('password'); ?>
                        </div>
                        <div class="error" id="password_validate"></div>
                        <div class="form-group">
                            <label for="password"> <?php echo translate('confirm_password'); ?> <small class="required mr-5px">*</small>
                                <i class="fa fa-info-circle" tabindex="0" data-html="true" data-toggle="popover" title="<b>Confirm Password</b> - Rules" data-content='<span class="d-block"><b> <?php echo translate('info'); ?> - </b></span><span class="d-block">- <?php echo translate('password_length'); ?></span>'></i></label>
                            <input type="password"  id="password2" name="password2" class="form-control" placeholder="<?php echo translate('confirm_password'); ?>">
                            <?php echo form_error('password2'); ?>
                        </div>
                        <div class="error" id="password2_validate"></div>
                        <div class="form-group">
                            <span><?php echo translate('agree_terms_conditions');?></span><br>
                        </div>

                        <div class="error" id="password2_validate"></div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-dark button_common"> <?php echo translate('register'); ?> </button>                        
                        </div>

                        <?php echo form_close(); ?>

                    </div>
                    <!--/Form with header-->
                </div>
            </div>
            <!-- End Col -->
        </div>
        <!--Row-->
    </div>
</section>

<script src="<?php echo $this->config->item('js_url'); ?>module/register.js" type="text/javascript"></script>
<?php include VIEWPATH . 'front/footer.php'; ?>

<script>

    function get_location(ci) {
        folder_name = "admin";
        var site_url = "<?php echo $this->config->item('site_url'); ?>";
        if (ci > 0) {
            $.ajax({
                url: site_url + folder_name + "/get-location/" + ci,
                type: "post",
                data: {token_id: csrf_token_name},
                beforeSend: function () {
                    $("#loadingmessage").show();
                },
                success: function (data) {
                    $('#city').html(data).material_select();
                    $("#loadingmessage").hide();
                }
            });
        }
    }
</script>
