<?php
include VIEWPATH . 'front/header.php';
?>
<!--Owl Carousel-->
<link href="<?php echo $this->config->item('js_url'); ?>owl-carousel/owl.theme.default.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo $this->config->item('js_url'); ?>owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo $this->config->item('js_url'); ?>owl-carousel/owl.carousel.min.js" type="text/javascript"></script>

<!--Date-Picker-->
<link href="<?php echo $this->config->item('css_url'); ?>datepicker.css" rel="stylesheet">
<script src="<?php echo $this->config->item('js_url'); ?>datepicker.js"></script>

<!-- Core theme CSS (includes Bootstrap)-->
<link href="<?php echo $this->config->item('css_url'); ?>styles.css" rel="stylesheet" />

<?php if (get_site_setting('is_display_top_menu') == "Y") {?>
<div class="container">
    <div class="row">
        <div class="col-md-12 mx-auto text-center">
                <?php $this->load->view('top_ads'); ?>
        </div>
    </div>
</div>
<?php } ?>

<!-- Masthead-->
<header class="masthead">
    <div class="container">
        <div class="masthead-heading text-uppercase">Online Booking System</div>
        <div class="masthead-subheading">For Service Based Providers</div>
        <a href="register" class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Free Now</a>
    </div>
</header>

<!-- Services-->
<section class="page-section" id="services">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Services</h2>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                <h4 class="my-3">Appointment</h4>
                <p class="text-muted">
                    For service providers and consultants who offer individual appointments and group workshops.
                </br></br>
                    Perfect for therapists, photographers, kids parties, car washing and more.
                </p>
            </div>
            <div class="col-md-4">
                <h4 class="my-3">Classes & Courses</h4>
                <p class="text-muted">
                    For schools and teachers who offer classes, courses, workshops and appointments.
                </br></br>
                    Perfect for language and cooking schools, art courses, yoga and more.
                </p>
            </div>
            <div class="col-md-4">
                <h4 class="my-3">Tours & Activities</h4>
                <p class="text-muted">For guided tour companies and outdoor activity providers who offer private and group tours.
                    </br></br>
                    Perfect for escape rooms, guided tours, charters and more.
                </p>
            </div>
        </div>
    </div>
</section>

<!-- Team-->
<section class="page-section bg-light" id="team">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase">Our Features</h2>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="team-member">
                    <h4>Schedule Appointments</h4>
                    <p class="text-muted">ZoomBee is a clean, simple tool with powerful calendar scheduling features that can handle all your salon appointment bookings. </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="team-member">
                    <h4>Accept Payments</h4>
                    <p class="text-muted">Accept online payments & deposits through a like PayPal and Stripe.</p>
                     </div>
            </div>
            <div class="col-lg-4">
                <div class="team-member">
                    <h4>Event Bookings</h4>
                    <p class="text-muted">You can choose the most convenient way for your clients to see your booking availability such as by class, time or staff.</p>
                   </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="team-member">
                    <h4>Unlimited Users</h4>
                    <p class="text-muted">We’ll never charge you for adding users, so your entire team can be on the same page. </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="team-member">
                    <h4>Mobile Friendly</h4>
                    <p class="text-muted">Never miss a beat, even away from your computer.</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="team-member">
                    <h4>FREE Package</h4>
                    <p class="text-muted">Try our FREE package with basic features. Is FREE forever!</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="container text-center">
                <a href="register" class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Sign Up For Free</a>
            </div>
        </div>
    </div>
</section>

<?php if (get_site_setting('enable_testimonial') == 'Y'): ?>
    <?php
    $get_app_testimonial = get_app_testimonial();
    if (count($get_app_testimonial) > 0):
        ?>
        <div class="py-4">
            <div class="container">
                <div class="event_category mt-20">
                    <h3 class="text-center"><?php echo translate('testimonial') ?></h3>
                </div>
                <div class="row">
                    <div class="owl-carousel owl-theme" id="testimonial-sliders">                
                        <?php
                        if (isset($get_app_testimonial) && !empty($get_app_testimonial)) {
                            foreach ($get_app_testimonial as $row) {

                                $testimonial_image = $row['image'];
                                if (isset($testimonial_image) && $testimonial_image != "") {
                                    if (file_exists(FCPATH . 'assets/uploads/category/' . $testimonial_image)) {
                                        $testimonial_image_path = base_url("assets/uploads/category/" . $testimonial_image);
                                    } else {
                                        $testimonial_image_path = base_url() . img_path . "/avatar.png";
                                    }
                                } else {
                                    $testimonial_image_path = base_url() . img_path . "/avatar.png";
                                }
                                ?>
                                <div class="testimonial">
                                    <div class="pic">
                                        <img src="<?php echo $testimonial_image_path; ?>" alt="">
                                    </div>
                                    <p class="description"><?php echo $row['details']; ?></p>
                                    <h3 class="title"><?php echo $row['name']; ?></h3>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>  
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
<?php include VIEWPATH . 'front/footer.php'; ?>
<script>
    $(document).ready(function () {
        if ($('#testimonial-sliders').length > 0) {
            $('#testimonial-sliders').owlCarousel({
                autoplay: false,
                loop: false,
                dots: true,
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 2,
                        slideBy: 2,
                    },

                }

            });

        }

    });
</script>