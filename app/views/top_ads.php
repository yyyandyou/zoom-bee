<?php
$top_ads_text = get_site_setting('top_ads_text');
$background_color = get_site_setting('header_color_code');

?>
<style>
    .mobile{display: none;}
    .desktop{display: block;}
    @media all and (max-width: 767px) {
        .mobile{display: block;}
        .desktop{display: none;}
    }
</style>
<div class="event_category">
    <div class="row">
        <?php
        $images_src = base_url(img_path . '/service.jpg');
        ?>
        <div class="col-md-12 col-12">
            <div class="event_c_img">
                <img style="height:80px" src="<?php echo $images_src; ?>" alt="category img" class="img-fluid" />
                <a href="<?php echo base_url('page/earn-money-from-zoombee'); ?>" title="<?php echo $top_ads_text;?>">
                    <div class="overlay header" style="background: <?php echo $background_color;?>; height:80px; padding-top:25px">
                        <h3 class="desktop text-center white-text"><?php echo $top_ads_text; ?>?</h3>
                        <h5 class="mobile text-center white-text"><b><?php echo $top_ads_text;?>?</b></h5>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>