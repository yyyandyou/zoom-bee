<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Downline extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('model_customer');
        set_time_zone();
        if ($this->login_type != 'V' || $this->login_type != 'A'):

        endif;
    }

    //show downline list
    public function index() {
        $data['title'] = translate('my_downline');
        $vendor_id = $this->login_id;
        $order = "created_on DESC";
        $downline = $this->model_customer->getData("app_admin", "*", "referral_id=" . $vendor_id . " AND type='V'", "", $order);
        $data['vendor_data'] = $downline;
        $this->load->view('admin/vendor/downline-list', $data);
    }

}

?>