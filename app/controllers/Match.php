<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//require APPPATH . '/libraries/REST_Controller.php';
//require APPPATH . '/libraries/Format.php';

class Match extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('model_event');
    }

    public function index()
    {
//        if (!$this->input->is_cli_request()) {
//            echo "This script can only be accessed via the command line" . PHP_EOL;
//            return;
//        }
        //get all event need to match
        $join_one = array(
            array(
                'table' => 'app_event',
                'condition' => 'app_event.id=app_event_book.event_id',
                'jointype' => 'INNER'
            ),
            array(
                'table' => 'app_event_ticket_type_booking',
                'condition' => 'app_event_book.id=app_event_ticket_type_booking.booking_id',
                'jointype' => 'INNER'
            ),
            array(
                'table' => 'app_event_ticket_type',
                'condition' => 'app_event_ticket_type.ticket_type_id=app_event_ticket_type_booking.ticket_type_id',
                'jointype' => 'INNER'
            )
        );
        $condition = "app_event.status='A' AND app_event.match_event='Y' AND ticket_type_id IN (select * from (SELECT ticket_type_id FROM app_event_ticket_type WHERE event_id = app_event.id LIMIT 1) temp_tab)";
        //$appointment_event = $this->model_event->getData("app_event_book", "app_event_book.event_id,app_event.id as event_id,app_event_book.customer_id, app_event_ticket_type_booking.ticket_type_id, app_event_ticket_type.ticket_type_title, app_event.title as title", $condition, $join_one, "", "app_event.id, app_event_book.customer_id, app_event_ticket_type_booking.ticket_type_id, app_event_ticket_type.ticket_type_title ");

        //var_dump($appointment_event);
    }

}
