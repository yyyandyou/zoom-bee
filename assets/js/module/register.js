
// User Registration Form

$(document).ready(function () {

    $("#Register_user").validate({
        ignore: [],
        rules: {
            first_name: {
                required: true,
            },
            last_name: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            dob_year: {
                required: true
            },
            dob_month: {
                required: true
            },
            dob_day: {
                required: true
            },
            country: {
                required: true
            },
            city: {
                required: true
            },
            interest:{
                required: true
            },
            race:{
                required: true
            },
            religion:{
                required: true
            },
            relationship:{
                required: true
            },
            children:{
                required: true
            },
            education:{
                required: true
            },
            preferred_language:{
                required: true
            },
            body_type:{
                required: true
            },
            smoke:{
                required: true
            },
            drink:{
                required: true
            },
            staying:{
                required: true
            },
            gender:{
                required: true
            },
            password: {
                required: true,
                minlength: 8
            },
            password2: {
                equalTo : "#password",
                minlength: 8
            }
        },
        messages: {
            first_name: {
                required: "Please enter your firstname",
            },
            last_name: {
                required: "Please enter your lastname",
            },
            email: {
                required: "Please enter your email",
                email: "Please enter a valid email"
            },
            dob_year: {
                required: "Please enter year"
            },
            dob_month: {
                required: "Please enter month"
            },
            dob_day: {
                required: "Please enter day"
            },
            country: {
                required: "Please enter country"
            },
            city: {
                required: "Please enter city"
            },
            interest: {
                required: "Please enter interest"
            },
            race: {
                required: "Please enter race"
            },
            religion: {
                required: "Please enter religion"
            },
            relationship: {
                required: "Please enter relationship"
            },
            children: {
                required: "Please enter have children"
            },
            education: {
                required: "Please enter education"
            },
            preferred_language: {
                required: "Please enter preferred language"
            },
            body_type: {
                required: "Please enter body type"
            },
            smoke: {
                required: "Please enter smoke"
            },
            drink: {
                required: "Please enter drink"
            },
            staying: {
                required: "Please enter staying"
            },
            gender: {
                required: "Please enter gender"
            },
            password: {
                required: "Please enter a password",
                minlength: "Please enter minimum 8 characters"
            },
            password2: {
                required: "Please enter a password",
                minlength: "Please enter minimum 8 characters"
            }
        },
    });
});

$(document).ready(function () {
    $('[data-toggle="popover"]').popover({
        placement: 'top',
        trigger: 'hover'
    });
});

