<?php

//set the environment to production after installation
if (!defined('ENVIRONMENT'))
    define('ENVIRONMENT', 'development');

//set the Google Analytic code
if (!defined('GOOGLE_ANALYTIC_CODE'))
    define('GOOGLE_ANALYTIC_CODE', 'UA-152399185-2');

$domain = $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
$domain = preg_replace('/index.php.*/', '', $domain); //remove everything after index.php
if (!empty($_SERVER['HTTPS'])) {
    $domain = 'https://' . $domain;
} else {
    $domain = 'http://' . $domain;
}

//database content
$hostname = "localhost";
$username = "efoodpan_bookmyslot";
$password = "efoodpan_bookmyslot";
$database = "efoodpan_bookmyslot";
